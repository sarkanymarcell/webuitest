﻿using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace WebdriverClass.Beadando
{
	class BadandoPageObjectTest : TestBase
	{
		[TestCaseSource("DistroSearchPageData")]
		public void DistroSearchPageTest(string selectInputString)
		{
			DistroSearchPage.SelectInput selectInput = (DistroSearchPage.SelectInput)Enum.Parse(typeof(DistroSearchPage.SelectInput), selectInputString);

			DistroSearchPage searchPage = HomePage.Navigate(Driver)
				.Maximize()
				.ClickDistroSearchLink()
				.GetDistroSearchPage();
			IWebElement target = searchPage.FindSelectInput(selectInput);

			Assert.NotNull(target);
		}

		[TestCaseSource("CommentsPageData")]
		public void CommentsPageTest(string imageString)
		{
			CommentsPage.Image image = (CommentsPage.Image)Enum.Parse(typeof(CommentsPage.Image), imageString);

			CommentsPage commentsPage = HomePage.Navigate(Driver)
				.Maximize()
				.ClickCommentsLink()
				.GetCommentsPage();
			IWebElement target = commentsPage.FindImage(image);

			Assert.NotNull(target);
		}

		[TestCaseSource("GlossaryPageData")]
		public void GlossaryPageTest(string linkString)
		{
			GlossaryPage.Link link = (GlossaryPage.Link)Enum.Parse(typeof(GlossaryPage.Link), linkString);

			GlossaryPage glossaryPage = HomePage.Navigate(Driver)
				.Maximize()
				.ClickGlossaryLink()
				.GetGlossaryPage();
			IWebElement target = glossaryPage.FindLink(link);

			Assert.NotNull(target);
		}

		static IEnumerable DistroSearchPageData()
		{
			return TestDataStore.TestData("search-page-data.xml", "selectInput");
		}

		static IEnumerable CommentsPageData()
		{
			return TestDataStore.TestData("comments-page-data.xml", "image");
		}

		static IEnumerable GlossaryPageData()
		{
			return TestDataStore.TestData("glossary-page-data.xml", "link");
		}

		[TearDown]
		public void AfterTest()
		{
			if (TestContext.CurrentContext.Result.Outcome != ResultState.Success)
			{
				string fileName = AppDomain.CurrentDomain.BaseDirectory
					+ TestContext.CurrentContext.Test.MethodName
					+ "_error.png";
				Screenshot screenshot = ((ITakesScreenshot)Driver).GetScreenshot();
				screenshot.SaveAsFile(fileName, ScreenshotImageFormat.Png);
				if (File.Exists(fileName))
				{
					Console.WriteLine("screenshot saved as: " + fileName);
				}
			}
		}
	}
}
