﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.Beadando
{
	class DistroSearchPage : BasePage
	{
		private WebDriverWait wait;

		public enum SelectInput
		{
			OsTypeSelect, OsCategorySelect, OsOriginSelect
		}

		public DistroSearchPage(IWebDriver webDriver) : base(webDriver)
		{
			this.wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(3));
		}

		public IWebElement FindSelectInput(SelectInput element)
		{
			By locator = By.CssSelector(GetCssSelectorOfElement(element));
			return wait.Until(x => x.FindElement(locator));
		}

		private string GetCssSelectorOfElement(SelectInput element)
		{
			return new string[] {
				"select[name='ostype']",
				"select[name='category']",
				"select[name='origin']"
			}[(int)element];
		}
	}
}
