﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.Beadando
{
	class GlossaryPage : BasePage
	{
		private WebDriverWait wait;

		public enum Link
		{
			DriverLink, EncryptionLink, HashLink, FailLink
		}

		public GlossaryPage(IWebDriver webDriver) : base(webDriver)
		{
			this.wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(3));
		}

		public IWebElement FindLink(Link link)
		{
			By locator = By.CssSelector(GetCssSelectorOfLink(link));
			return wait.Until(x => x.FindElement(locator));
		}

		private string GetCssSelectorOfLink(Link link)
		{
			return new string[] {
				"a[href='#driver']",
				"a[href='#encryption']",
				"a[href='#hash']",
				"a[href='not_found_locator']"
			}[(int)link];
		}
	}
}
