﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.Beadando
{
	class HomePage : BasePage
	{
		//public IWebElement DistroInfoLink => Driver.FindElement(By.XPath("//a[@href='fedora']"));
		private IWebElement DistroSearchLink => Driver.FindElement(By.CssSelector("a[href='search.php']"));
		private IWebElement CommentsLink => Driver.FindElement(By.CssSelector("a[href='weekly.php?issue=current&mode=67#comments']"));
		private IWebElement GlossaryLink => Driver.FindElement(By.CssSelector("a[href='dwres.php?resource=glossary']"));

		public HomePage(IWebDriver webDriver) : base(webDriver)
		{
		}

		public static HomePage Navigate(IWebDriver webDriver)
		{
			webDriver.Url = "https://distrowatch.com/";
			return new HomePage(webDriver);
		}

		public HomePage Maximize()
		{
			Driver.Manage().Window.Maximize();
			return new HomePage(Driver);
		}

		public HomePage ClickDistroSearchLink()
		{
			DistroSearchLink.Click();
			return new HomePage(Driver);
		}

		public HomePage ClickCommentsLink()
		{
			CommentsLink.Click();
			return new HomePage(Driver);
		}

		public HomePage ClickGlossaryLink()
		{
			GlossaryLink.Click();
			return new HomePage(Driver);
		}

		public DistroSearchPage GetDistroSearchPage()
		{
			return new DistroSearchPage(Driver);
		}

		public CommentsPage GetCommentsPage()
		{
			return new CommentsPage(Driver);
		}

		public GlossaryPage GetGlossaryPage()
		{
			return new GlossaryPage(Driver);
		}
	}
}
