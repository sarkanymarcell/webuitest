﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace WebdriverClass.Beadando
{
	static class TestDataStore
	{
		public static IEnumerable TestData(string file, string key)
		{
			var doc = XElement.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory) + "\\" + file);
			return
				from vars in doc.Descendants("testData")
				let link = vars.Attribute(key).Value
				select new object[] { link };
		}
	}
}
