﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.Beadando
{
	class CommentsPage : BasePage
	{
		private WebDriverWait wait;

		public enum Image
		{
			BitcoinImage, MoneroImage, PayPalImage
		}

		public CommentsPage(IWebDriver webDriver) : base(webDriver)
		{
			this.wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(3));
		}

		public IWebElement FindImage(Image image)
		{
			By locator = By.CssSelector(GetCssSelectorOfImage(image));
			return wait.Until(x => x.FindElement(locator));
		}

		private string GetCssSelectorOfImage(Image image)
		{
			return new string[] {
				"img[src='images/other/bitcoinqr-for-dww.png']",
				"img[src='images/other/moneroqr-for-dww.png']",
				"img[src='images/other/tip-with-paypal.gif']"
			}[(int)image];
		}
	}
}
